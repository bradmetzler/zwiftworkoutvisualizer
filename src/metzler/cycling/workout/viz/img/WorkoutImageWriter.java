package metzler.cycling.workout.viz.img;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.Path2D;
import java.awt.geom.QuadCurve2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import metzler.cycling.workout.viz.WorkoutVisualizerIOConfig;
import metzler.cycling.workout.viz.data.TimeInZone;
import metzler.cycling.workout.viz.data.Workout;
import metzler.cycling.workout.viz.data.WorkoutHelper;
import metzler.cycling.workout.viz.data.WorkoutSegment;
import metzler.cycling.workout.viz.data.WorkoutSegment.Type;

public class WorkoutImageWriter {
	private static final Color z1Color = new Color(0xB6D7A8);
	private static final Color z2Color = new Color(0xA4C2F4);
	private static final Color z3Color = new Color(0xFFE598);
	private static final Color z4Color = new Color(0xF9CB9C);
	private static final Color z5Color = new Color(0xEA9899);
	private static final Color z6Color = new Color(0xDD7F6B);
	private static final Color frColor = new Color(0x666666);

	private static final Color z1ColorBg = new Color(z1Color.getRed(), z1Color.getGreen(), z1Color.getBlue(), 35);
	private static final Color z2ColorBg = new Color(z2Color.getRed(), z2Color.getGreen(), z2Color.getBlue(), 35);
	private static final Color z3ColorBg = new Color(z3Color.getRed(), z3Color.getGreen(), z3Color.getBlue(), 35);
	private static final Color z4ColorBg = new Color(z4Color.getRed(), z4Color.getGreen(), z4Color.getBlue(), 35);
	private static final Color z5ColorBg = new Color(z5Color.getRed(), z5Color.getGreen(), z5Color.getBlue(), 35);
	private static final Color z6ColorBg = new Color(z6Color.getRed(), z6Color.getGreen(), z6Color.getBlue(), 35);
	
	private static final boolean drawHashes = true;
	private static final Color hashColor = new Color(0xD8D8D8);
	private static final BasicStroke hashStroke = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10, new float[]{5}, 0);

	private static final boolean renderTitle = false;
	private static final int imgPadding = 10;
	private static final int graphWidth = 655;
	private static final int graphHeight = 210;
	private static final int width = graphWidth + 125 + (imgPadding * 2);	//Extra space for duration and time-in-zone
	private static final int height = graphHeight + (renderTitle ? 25 : 0) + (imgPadding * 2);
	private static final int timeInZoneTotalBarWidth = 100;

	private static final int blockGap = 1;
	private static final int rectRadius = 10;

	private static int zoneHeight = 0;
	private static int topBuffer = 0;
	private static int secPerPx = 0;

	public static boolean writeImage(Workout workout, String fileName) {
		System.out.println("  Writing image for: " + workout.name);
		
		zoneHeight = Math.floorDiv(graphHeight, 6);
		topBuffer = height - imgPadding - (zoneHeight * 6);
		int totalBlockGapPx = (workout.blockizeTheIntervals().size() + 1) * blockGap;
		secPerPx = (int) Math.ceil((float) workout.totalTime / (graphWidth - totalBlockGapPx));
		int gutterPixels = (graphWidth - (workout.totalTime / secPerPx) - totalBlockGapPx) / 2; // Each side

		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = img.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		// Draw background
		g.setBackground(Color.WHITE);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);

		// Draw Z1 background (with rounded corners)
		g.setColor(z1ColorBg);
		g.fillRoundRect(imgPadding, (zoneHeight * 4) + topBuffer, graphWidth, zoneHeight * 2, rectRadius, rectRadius); // Extra large for rounded corners
		g.setColor(Color.WHITE);
		g.fillRect(imgPadding, (zoneHeight * 4) + topBuffer, graphWidth, zoneHeight); // Erase top half of Z1 background (because of top rounded corners)

		// Draw Z6 background (with rounded corners)
		g.setColor(z6ColorBg);
		g.fillRoundRect(imgPadding, topBuffer, graphWidth, zoneHeight * 2, rectRadius, rectRadius); // Extra large for rounded corners
		g.setColor(Color.WHITE);
		g.fillRect(imgPadding, zoneHeight + topBuffer, graphWidth, zoneHeight); // Erase bottom half of Z6 background (because of bottom rounded corners)

		// Draw Z2 background
		g.setColor(z2ColorBg);
		g.fillRect(imgPadding, (zoneHeight * 4) + topBuffer, graphWidth, zoneHeight);

		// Draw Z3 background
		g.setColor(z3ColorBg);
		g.fillRect(imgPadding, (zoneHeight * 3) + topBuffer, graphWidth, zoneHeight);

		// Draw Z4 background
		g.setColor(z4ColorBg);
		g.fillRect(imgPadding, (zoneHeight * 2) + topBuffer, graphWidth, zoneHeight);

		// Draw Z5 background
		g.setColor(z5ColorBg);
		g.fillRect(imgPadding, zoneHeight + topBuffer, graphWidth, zoneHeight);

		//Draw hashes
		if(drawHashes) {
			g.setStroke(hashStroke);
			g.setColor(hashColor);
			g.drawLine(imgPadding, zoneHeight + topBuffer, imgPadding + graphWidth, zoneHeight + topBuffer);
			g.drawLine(imgPadding, (zoneHeight * 2) + topBuffer, imgPadding + graphWidth, (zoneHeight * 2) + topBuffer);
			g.drawLine(imgPadding, (zoneHeight * 3) + topBuffer, imgPadding + graphWidth, (zoneHeight * 3) + topBuffer);
			g.drawLine(imgPadding, (zoneHeight * 4) + topBuffer, imgPadding + graphWidth, (zoneHeight * 4) + topBuffer);
			g.drawLine(imgPadding, (zoneHeight * 5) + topBuffer, imgPadding + graphWidth, (zoneHeight * 5) + topBuffer);
		}
		
		// Draw title
		if(renderTitle) {
			g.setColor(Color.BLACK);
			g.setFont(new Font("SansSerif", Font.BOLD, 18));
			g.drawString(workout.name, imgPadding, 25);
		}

		// Draw duration
		g.setFont(new Font("SansSerif", Font.BOLD, 14));
		g.setColor(Color.BLACK);
		g.drawString("Duration:", graphWidth + (imgPadding * 2), topBuffer + 10);
		g.setFont(new Font("SansSerif", Font.PLAIN, 14));
		g.drawString(WorkoutHelper.friendlyDuration(workout.totalTime), graphWidth + (imgPadding * 2) + 70, topBuffer + 10);
		
		//Draw TSS
		g.setFont(new Font("SansSerif", Font.BOLD, 14));
		g.setColor(Color.BLACK);
		g.drawString("TSS:", graphWidth + (imgPadding * 2) + 34, topBuffer + 24);
		g.setFont(new Font("SansSerif", Font.PLAIN, 14));
		g.drawString(workout.getTssString(), graphWidth + (imgPadding * 2) + 70, topBuffer + 24);

		// Draw time-in-zone graph
		g.setColor(z1Color);
		g.fillRect(graphWidth + (imgPadding * 2) + 41, topBuffer + 34, (int) (timeInZoneTotalBarWidth * ((float) workout.z1Time / workout.totalTime)), 13);
		g.setColor(z2Color);
		g.fillRect(graphWidth + (imgPadding * 2) + 41, topBuffer + 49, (int) (timeInZoneTotalBarWidth * ((float) workout.z2Time / workout.totalTime)), 13);
		g.setColor(z3Color);
		g.fillRect(graphWidth + (imgPadding * 2) + 41, topBuffer + 64, (int) (timeInZoneTotalBarWidth * ((float) workout.z3Time / workout.totalTime)), 13);
		g.setColor(z4Color);
		g.fillRect(graphWidth + (imgPadding * 2) + 41, topBuffer + 79, (int) (timeInZoneTotalBarWidth * ((float) workout.z4Time / workout.totalTime)), 13);
		g.setColor(z5Color);
		g.fillRect(graphWidth + (imgPadding * 2) + 41, topBuffer + 94, (int) (timeInZoneTotalBarWidth * ((float) workout.z5Time / workout.totalTime)), 13);
		g.setColor(z6Color);
		g.fillRect(graphWidth + (imgPadding * 2) + 41, topBuffer + 109, (int) (timeInZoneTotalBarWidth * ((float) workout.z6Time / workout.totalTime)), 13);

		// Draw time-in-zone stats
		g.setFont(new Font("SansSerif", Font.BOLD, 12));
		g.setColor(Color.GRAY);
		g.drawString("Z1:", graphWidth + (imgPadding * 2) + 20, topBuffer + 45);
		g.drawString("Z2:", graphWidth + (imgPadding * 2) + 20, topBuffer + 60);
		g.drawString("Z3:", graphWidth + (imgPadding * 2) + 20, topBuffer + 75);
		g.drawString("Z4:", graphWidth + (imgPadding * 2) + 20, topBuffer + 90);
		g.drawString("Z5:", graphWidth + (imgPadding * 2) + 20, topBuffer + 105);
		g.drawString("Z6:", graphWidth + (imgPadding * 2) + 20, topBuffer + 120);
		g.setFont(new Font("SansSerif", Font.PLAIN, 12));
		g.drawString(WorkoutHelper.friendlyDuration(workout.z1Time), graphWidth + (imgPadding * 2) + 41, topBuffer + 45);
		g.drawString(WorkoutHelper.friendlyDuration(workout.z2Time), graphWidth + (imgPadding * 2) + 41, topBuffer + 60);
		g.drawString(WorkoutHelper.friendlyDuration(workout.z3Time), graphWidth + (imgPadding * 2) + 41, topBuffer + 75);
		g.drawString(WorkoutHelper.friendlyDuration(workout.z4Time), graphWidth + (imgPadding * 2) + 41, topBuffer + 90);
		g.drawString(WorkoutHelper.friendlyDuration(workout.z5Time), graphWidth + (imgPadding * 2) + 41, topBuffer + 105);
		g.drawString(WorkoutHelper.friendlyDuration(workout.z6Time), graphWidth + (imgPadding * 2) + 41, topBuffer + 120);

		// Draw workout blocks
		int x = imgPadding + gutterPixels + blockGap;
		for (WorkoutSegment segment : workout.blockizeTheIntervals()) {
			int segWidth = segment.durationSeconds / secPerPx;

			if (Type.BLOCK == segment.type) {
				int segHeight = 0;

				if (segment.power >= WorkoutHelper.z6) {
					g.setColor(z6Color);
					segHeight = (int) Math.ceil((zoneHeight * 5) + (zoneHeight * WorkoutHelper.percentageIntoZone(segment.power)));
				} else if (segment.power >= WorkoutHelper.z5) {
					g.setColor(z5Color);
					segHeight = (int) Math.ceil((zoneHeight * 4) + (zoneHeight * WorkoutHelper.percentageIntoZone(segment.power)));
				} else if (segment.power >= WorkoutHelper.z4) {
					g.setColor(z4Color);
					segHeight = (int) Math.ceil((zoneHeight * 3) + (zoneHeight * WorkoutHelper.percentageIntoZone(segment.power)));
				} else if (segment.power >= WorkoutHelper.z3) {
					g.setColor(z3Color);
					segHeight = (int) Math.ceil((zoneHeight * 2) + (zoneHeight * WorkoutHelper.percentageIntoZone(segment.power)));
				} else if (segment.power >= WorkoutHelper.z2) {
					g.setColor(z2Color);
					segHeight = (int) Math.ceil(zoneHeight + (zoneHeight * WorkoutHelper.percentageIntoZone(segment.power)));
				} else if (segment.power >= WorkoutHelper.z1) {
					g.setColor(z1Color);
					segHeight = (int) Math.ceil(zoneHeight * WorkoutHelper.percentageIntoZone(segment.power));
				}

				g.fillRoundRect(x, (zoneHeight * 6) + topBuffer - segHeight, segWidth, segHeight, rectRadius, rectRadius);
				x += segWidth + blockGap;
			} else if (Type.RAMP == segment.type) {
				int segHeightStart = 0;
				int segHeightEnd = 0;

				if (segment.power >= WorkoutHelper.z6) {
					segHeightStart = (int) Math.ceil((zoneHeight * 5) + (zoneHeight * WorkoutHelper.percentageIntoZone(segment.power)));
				} else if (segment.power >= WorkoutHelper.z5) {
					segHeightStart = (int) Math.ceil((zoneHeight * 4) + (zoneHeight * WorkoutHelper.percentageIntoZone(segment.power)));
				} else if (segment.power >= WorkoutHelper.z4) {
					segHeightStart = (int) Math.ceil((zoneHeight * 3) + (zoneHeight * WorkoutHelper.percentageIntoZone(segment.power)));
				} else if (segment.power >= WorkoutHelper.z3) {
					segHeightStart = (int) Math.ceil((zoneHeight * 2) + (zoneHeight * WorkoutHelper.percentageIntoZone(segment.power)));
				} else if (segment.power >= WorkoutHelper.z2) {
					segHeightStart = (int) Math.ceil(zoneHeight + (zoneHeight * WorkoutHelper.percentageIntoZone(segment.power)));
				} else if (segment.power >= WorkoutHelper.z1) {
					segHeightStart = (int) Math.ceil(zoneHeight * WorkoutHelper.percentageIntoZone(segment.power));
				}

				if (segment.power2 >= WorkoutHelper.z6) {
					segHeightEnd = (int) Math.ceil((zoneHeight * 5) + (zoneHeight * WorkoutHelper.percentageIntoZone(segment.power2)));
				} else if (segment.power2 >= WorkoutHelper.z5) {
					segHeightEnd = (int) Math.ceil((zoneHeight * 4) + (zoneHeight * WorkoutHelper.percentageIntoZone(segment.power2)));
				} else if (segment.power2 >= WorkoutHelper.z4) {
					segHeightEnd = (int) Math.ceil((zoneHeight * 3) + (zoneHeight * WorkoutHelper.percentageIntoZone(segment.power2)));
				} else if (segment.power2 >= WorkoutHelper.z3) {
					segHeightEnd = (int) Math.ceil((zoneHeight * 2) + (zoneHeight * WorkoutHelper.percentageIntoZone(segment.power2)));
				} else if (segment.power2 >= WorkoutHelper.z2) {
					segHeightEnd = (int) Math.ceil(zoneHeight + (zoneHeight * WorkoutHelper.percentageIntoZone(segment.power2)));
				} else if (segment.power2 >= WorkoutHelper.z1) {
					segHeightEnd = (int) Math.ceil(zoneHeight * WorkoutHelper.percentageIntoZone(segment.power2));
				}

				// Corners of the overall ramp block (before sub-dividing by zone)
				AffineTransform at = new AffineTransform();
				Point rampTl = new Point(x, (zoneHeight * 6) + topBuffer - segHeightStart);
				Point rampTr = new Point(x + segWidth, (zoneHeight * 6) + topBuffer - segHeightEnd);
				//Point rampBl = new Point(x, (zoneHeight * 6) + topBuffer);
				//Point rampBr = new Point(x + segWidth, (zoneHeight * 6) + topBuffer);

				boolean roundLeft = false;
				boolean roundRight = false;
				int initialX = x;
				for(int i = 0; i < segment.durationInZones.size(); i++) {
					TimeInZone tiz = segment.durationInZones.get(i);
					
					roundLeft = (i == 0);
					roundRight = (i == (segment.durationInZones.size() - 1));
					
					float percOfRamp = (float)tiz.time / segment.durationSeconds;
					int subWidth = (int)(segWidth * percOfRamp);
					
					if(roundRight && ((x + subWidth - initialX) < segWidth)) {
						//This is the final segment of the ramp and, due to rounding, we are a
						//pixel or two short of how long the total ramp should be. Correct it here.
						subWidth += segWidth - (x + subWidth - initialX);
					}
					
					float subStartPerc = (float)(x - initialX) / segWidth;
					float subEndPerc = (float)(x - initialX + subWidth) / segWidth;
					
					Point tl = new Point(x, pointOnLine(rampTl, rampTr, subStartPerc).y);
					Point tr = new Point(x + subWidth, pointOnLine(rampTl, rampTr, subEndPerc).y);
					Point bl = new Point(x, (zoneHeight * 6) + topBuffer);
					Point br = new Point(x + subWidth, (zoneHeight * 6) + topBuffer);
					
					Point l1 = new Point(bl.x, bl.y - 5);	//Low point of left line for curve
					Point l2 = new Point(tl.x, tl.y + 5);	//High point of left line for curve
					Point t1 = pointOnLine(tl, tr, ((float)5 / subWidth));	//Left side of top line for curve
					Point t2 = pointOnLine(tl, tr, ((float)(subWidth - 5) / subWidth));	//Right side of top line for curve
					Point r1 = new Point(tr.x, tr.y + 5);	//High point of right line for curve
					Point r2 = new Point(br.x, br.y - 5);	//Low point of right line for curve
					Point b1 = new Point(br.x - 5, br.y);	//Right side of bottom line for curve
					Point b2 = new Point(bl.x + 5, bl.y);	//Left side of bottom line for curve
					QuadCurve2D tlCurve = new QuadCurve2D.Double(l2.x, l2.y, tl.x, tl.y, t1.x, t1.y);
					QuadCurve2D trCurve = new QuadCurve2D.Double(t2.x, t2.y, tr.x, tr.y, r1.x, r1.y);
					QuadCurve2D brCurve = new QuadCurve2D.Double(r2.x, r2.y, br.x, br.y, b1.x, b1.y);
					QuadCurve2D blCurve = new QuadCurve2D.Double(b2.x, b2.y, bl.x, bl.y, l1.x, l1.y);
					
					//Set subdivision color based on zone
					if(tiz.zone == 6)
						g.setColor(z6Color);
					else if(tiz.zone == 5)
						g.setColor(z5Color);
					else if(tiz.zone == 4)
						g.setColor(z4Color);
					else if(tiz.zone == 3)
						g.setColor(z3Color);
					else if(tiz.zone == 2)
						g.setColor(z2Color);
					else
						g.setColor(z1Color);
					
					Path2D seg = new Path2D.Double();
					
					//Start in bottom left and draw to top left
					if(roundLeft) {
						seg.moveTo(b2.x, b2.y);
						seg.append(blCurve.getPathIterator(at), true);
						seg.moveTo(l2.x, l2.y);
						seg.append(tlCurve.getPathIterator(at), true);
					} else {
						seg.moveTo(bl.x, bl.y);
						seg.lineTo(tl.x, tl.y);
					}
					
					//Draw to top right and bottom right
					if(roundRight) {
						seg.lineTo(t2.x, t2.y);
						seg.append(trCurve.getPathIterator(at), true);
						seg.lineTo(r2.x, r2.y);
						seg.append(brCurve.getPathIterator(at), true);
					} else {
						seg.lineTo(tr.x, tr.y);
						seg.lineTo(br.x, br.y);
					}
					
					//Draw back to bottom left to close the polygon
					if(roundLeft) {
						seg.lineTo(b2.x, b2.y);
						seg.append(blCurve.getPathIterator(at), true);
					} else {
						seg.lineTo(bl.x, bl.y);
					}
					
					//Close the polygon
					seg.closePath();

					g.fill(seg);
					
					x += subWidth;
				}

				x += blockGap;
			} else if (Type.FREERIDE == segment.type) {
				//Top of free-ride segment should be (arbitrarily) halfway up zone 3
				int segHeight = (int) Math.ceil((zoneHeight * 2) + (zoneHeight * 0.5));

				//Corners of the free-ride block
				AffineTransform at = new AffineTransform();
				Point freeTl = new Point(x, (zoneHeight * 6) + topBuffer - segHeight);
				Point freeTr = new Point(x + segWidth, (zoneHeight * 6) + topBuffer - segHeight);
				Point freeBl = new Point(x, (zoneHeight * 6) + topBuffer);
				Point freeBr = new Point(x + segWidth, (zoneHeight * 6) + topBuffer);

				g.setColor(frColor);
										
				Point l1 = new Point(freeBl.x, freeBl.y - 5);	//Low point of left line for curve
				Point r2 = new Point(freeBr.x, freeBr.y - 5);	//Low point of right line for curve
				Point b1 = new Point(freeBr.x - 5, freeBr.y);	//Right side of bottom line for curve
				Point b2 = new Point(freeBl.x + 5, freeBl.y);	//Left side of bottom line for curve
				QuadCurve2D brCurve = new QuadCurve2D.Double(r2.x, r2.y, freeBr.x, freeBr.y, b1.x, b1.y);
				QuadCurve2D blCurve = new QuadCurve2D.Double(b2.x, b2.y, freeBl.x, freeBl.y, l1.x, l1.y);
				
				Path2D frBlock = new Path2D.Double();
				
				//Start in top-right and draw to bottom-right
				frBlock.moveTo(freeTr.x, freeTr.y);
				frBlock.lineTo(r2.x, r2.y);
				
				//Draw bottom-right curve
				if(segWidth >= 10) {
					frBlock.append(brCurve.getPathIterator(at), true);
				} else {
					frBlock.lineTo(freeBr.x, freeBr.y);
					frBlock.lineTo(b1.x, b1.y);
				}
				
				//Draw to bottom-left
				frBlock.lineTo(b2.x, b2.y);
				
				//Draw bottom-left curve
				if(segWidth >= 10) {
					frBlock.append(blCurve.getPathIterator(at), true);
				} else {
					frBlock.lineTo(freeBl.x, freeBl.y);
					frBlock.lineTo(l1.x, l1.y);
				}
				
				//Draw to top-left
				frBlock.lineTo(freeTl.x, freeTl.y);
				
				//Draw the bezier curve
				if(segWidth >= 10) {
					CubicCurve2D bezier = new CubicCurve2D.Double(freeTl.x, freeTl.y, freeTl.x + 10, freeTl.y + 15, freeTr.x - 10, freeTr.y - 15, freeTr.x, freeTr.y);
					frBlock.append(bezier.getPathIterator(at), true);
				} else {
					frBlock.lineTo(freeTr.x, freeTr.y);
				}
				
				//Close the polygon
				frBlock.closePath();	
				g.fill(frBlock);
				
				x += segWidth + blockGap;
			}
		}

		try {
			File png = new File(WorkoutVisualizerIOConfig.outputLocation + "/" + fileName + ".png");
			ImageIO.write(img, "png", png);
			return true;
		} catch (Exception e) {
			e.printStackTrace(System.out);
			return false;
		}
	}
	
	private static Point pointOnLine(Point p1, Point p2, float progressDownLine) {
		int x = (int)(((p2.x - p1.x) * progressDownLine) + p1.x);
		int y = (int)(((p2.y - p1.y) * progressDownLine) + p1.y);
		return new Point(x, y);
	}
}