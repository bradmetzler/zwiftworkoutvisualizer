package metzler.cycling.workout.viz.sheets;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import metzler.cycling.workout.viz.WorkoutVisualizerIOConfig;
import metzler.cycling.workout.viz.data.Workout;
import metzler.cycling.workout.viz.data.WorkoutHelper;
import metzler.cycling.workout.viz.data.WorkoutSegment;
import metzler.cycling.workout.viz.data.WorkoutSegment.Type;

public class WorkoutSheetsWriter {
	
	private static class DetailsBreakdown {
		public String breakdown;
		public int numRows = 0;
	}
	
	public static boolean writeSheetsText(Workout workout, String fileName) {
		System.out.println("  Writing sheet for: " + workout.name);
		
		DetailsBreakdown breakdown = getBreakdown(workout);
		
		try {
			File txt = new File(WorkoutVisualizerIOConfig.outputLocation + "/" + fileName + ".txt");
			BufferedWriter writer = new BufferedWriter(new FileWriter(txt));
			
			//Number of rows required for this workout in workout spreadsheet (breakdown rows + header row + 11 rows for the image)
			writer.write((breakdown.numRows + 1 + 11) + " rows");
			writer.write("\n\n");
			
			//Title row
			writer.write("Paste into column B\n");
			writer.write("={\"" + workout.name + "\",\"\",\"\",\"\",\"\",\"\",\"" + WorkoutHelper.friendlyDuration(workout.totalTime) + "\",\"" + workout.getTssString() + " TSS\",\"\",\"\",\"" + workout.description + "\"}");
			writer.write("\n\n");
			
			//Image area
			writer.write("Paste into column C below workout title, then merge to place image\n");
			writer.write("={\"C\",\"- to ->\",\"K\";\"C\",\"- to ->\",\"K\";\"C\",\"- to ->\",\"K\";\"C\",\"- to ->\",\"K\";\"C\",\"- to ->\",\"K\";\"C\",\"- to ->\",\"K\";\"C\",\"- to ->\",\"K\";\"C\",\"- to ->\",\"K\";\"C\",\"- to ->\",\"K\";\"C\",\"- to ->\",\"K\";\"C\",\"- to ->\",\"K\"}");
			writer.write("\n\n");
			
			//Breakdown
			writer.write("Paste into column C below image\n");
			writer.write(breakdown.breakdown);
			
			writer.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace(System.out);
			return false;
		}
	}
	
	//Sample
	//={"1x of","","";"","3m",51;"","1m",85;"","2m",51;"","3m",91;"","3m",51;"10x of","","";"","1m",114;"","2m",65;"1x of","","";"","3m",51;"10m ramp","","";"","",100;"","",89;"1x of","","";"","5m",51}
	private static DetailsBreakdown getBreakdown(Workout workout) {
		DetailsBreakdown breakdown = new DetailsBreakdown();
		
		boolean inA1x = false;	//Whether we're already under a "1x" heading
		
		StringBuilder s = new StringBuilder();
		s.append("={");
		for(WorkoutSegment segment : workout.segments) {
			if(Type.BLOCK == segment.type) {
				if(!inA1x) {
					//Need to print a new "1x" heading
					inA1x = true;
					s.append("\"1x of\",\"\",\"\";");
					breakdown.numRows++;
				}
				s.append("\"\",\"" + WorkoutHelper.friendlyDuration(segment.durationSeconds) + "\"," + segment.power + ";");
				breakdown.numRows++;
			} else if(Type.FREERIDE == segment.type) {
				if(!inA1x) {
					//Need to print a new "1x" heading
					inA1x = true;
					s.append("\"1x of\",\"\",\"\";");
					breakdown.numRows++;
				}
				s.append("\"\",\"" + WorkoutHelper.friendlyDuration(segment.durationSeconds) + "\",0;");
				breakdown.numRows++;
			} else if(Type.RAMP == segment.type) {
				inA1x = false;
				s.append("\"" + WorkoutHelper.friendlyDuration(segment.durationSeconds) + " ramp\",\"\",\"\";");
				s.append("\"\",\"\"," + segment.power + ";");
				s.append("\"\",\"\"," + segment.power2 + ";");
				breakdown.numRows += 3;
			} else if(Type.INTERVALS == segment.type) {
				inA1x = false;
				s.append("\"" + segment.repeat + "x of\",\"\",\"\";");
				s.append("\"\",\"" + WorkoutHelper.friendlyDuration(segment.durationOn) + "\"," + segment.power + ";");
				s.append("\"\",\"" + WorkoutHelper.friendlyDuration(segment.durationOff) + "\"," + segment.power2 + ";");
				breakdown.numRows += 3;
			}
		}
		s.deleteCharAt(s.length() - 1);
		s.append("}");
		breakdown.breakdown = s.toString();
		
		return breakdown;
	}
}
