package metzler.cycling.workout.viz;

import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import metzler.cycling.workout.viz.data.Workout;
import metzler.cycling.workout.viz.data.WorkoutReader;
import metzler.cycling.workout.viz.img.WorkoutImageWriter;
import metzler.cycling.workout.viz.sheets.WorkoutSheetsWriter;

public class WorkoutVisualizer {
	public static void main(String... args) {
		try (Stream<Path> files = Files.walk(Paths.get(WorkoutVisualizerIOConfig.inputLocation), 2, FileVisitOption.FOLLOW_LINKS)) {
			List<Path> workoutFiles = files.filter(Files::isRegularFile).filter(x -> x.getFileName().toString().endsWith(".zwo")).collect(Collectors.toList());
			
			int numWorkouts = workoutFiles.size();
			System.out.println("Found " + numWorkouts + " ZWO files...");
			System.out.println();
			for(int i = 0; i <numWorkouts; i++) {
				System.out.println("Processing workout " + (i + 1) + " of " + numWorkouts);
				
				Path workoutFile = workoutFiles.get(i);
				Workout workout = WorkoutReader.readWorkout(workoutFile);

				if (workout == null) {
					System.out.println("  Could not parse workout file: " + workoutFile.getFileName());
					continue;
				}

				String fileName = workoutFile.getFileName().toString();
				fileName = fileName.substring(0, fileName.lastIndexOf("."));
				
				boolean imageSuccess = WorkoutImageWriter.writeImage(workout, fileName);
				if (!imageSuccess) {
					System.out.println("  Could not write image for: " + workoutFile.getFileName());
					continue;
				}
				
				boolean sheetsSuccess = WorkoutSheetsWriter.writeSheetsText(workout, fileName);
				if(!sheetsSuccess) {
					System.out.println("  Could not write sheet for: " + workoutFile.getFileName());
					continue;
				}
			}
			System.out.println("Done!");
		} catch (Exception e) {
			System.out.println("Something went wrong!");
			e.printStackTrace(System.out);
		}
	}
}
