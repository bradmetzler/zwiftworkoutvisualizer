package metzler.cycling.workout.viz.data;

public class TimeInZone {
	public int zone;
	public int time;
	public TimeInZone(int zone) {
		this.zone = zone;
		this.time = 0;
	}
	public TimeInZone(int zone, int time) {
		this.zone = zone;
		this.time = time;
	}
}
