package metzler.cycling.workout.viz.data;

public class WorkoutHelper {
  public static int z6 = 121;
  public static int z5 = 106;
  public static int z4 = 91;
  public static int z3 = 76;
  public static int z2 = 55;
  public static int z1 = 0;

  public static String friendlyDuration(int seconds) {
    if(seconds <= 0) {
    	return "-";
    }
	  
	int h = seconds / 3600;
    int m = (seconds - h * 3600) / 60;
    int s = seconds % 60;

    StringBuilder x = new StringBuilder();

    if (h > 0) {
      x.append(h);
      x.append("h ");
    }
    if (m > 0) {
      x.append(m);
      x.append("m ");
    }
    if (s > 0) {
      if (s < 10) {
        x.append("0");
      }
      x.append(s);
      x.append("s");
    }

    if (x.length() == 0) {
      x.append("0s");
    }

    return x.toString().trim();
  }
  
  public static float percentageIntoZone(int power) {
	  int zMin = 0;
	  int zMax = 0;
	  
	  if (power >= z6) {
		  zMin = z6;
		  zMax = 175;
	  } else if (power >= z5) {
		  zMin = z5;
		  zMax = z6 - 1;
	  } else if (power >= z4) {
		  zMin = z4;
		  zMax = z5 - 1;
	  } else if (power >= z3) {
		  zMin = z3;
		  zMax = z4 - 1;
	  } else if (power >= z2) {
		  zMin = z2;
		  zMax = z3 - 1;
	  } else if (power >= z1) {
		  zMin = z1;
		  zMax = z2 - 1;
	  }
	  
	  float result = ((float)power - zMin) / (zMax - zMin);
	  if(result > 1)
		  return 1;
	  else
		  return result;
  }
}
