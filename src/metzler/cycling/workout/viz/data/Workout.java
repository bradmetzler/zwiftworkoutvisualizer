package metzler.cycling.workout.viz.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import metzler.cycling.workout.viz.data.WorkoutSegment.Type;

public class Workout {
	public String name = null;
	public String description = null;
	public List<WorkoutSegment> segments = new ArrayList<>();
	private List<WorkoutSegment> blockized = null;

	public int tss = 0;
	
	public int totalTime = 0;
	public int z1Time = 0;
	public int z2Time = 0;
	public int z3Time = 0;
	public int z4Time = 0;
	public int z5Time = 0;
	public int z6Time = 0;

	public List<WorkoutSegment> blockizeTheIntervals() {
		if(blockized == null) {
			blockized = new ArrayList<>();
			
			for(WorkoutSegment segment : segments) {
				if(segment.type == Type.INTERVALS) {
					blockized.addAll(segment.blockizeTheIntervals());
				} else {
					blockized.add(segment);
				}
			}			
		}
		
		return blockized;
	}
	
	public void finish() {
		//Calculate TSS, total time, and time-in-zone
		
		NormalizedPowerCalculator npCalc = new NormalizedPowerCalculator();
		
		for (WorkoutSegment segment : blockizeTheIntervals()) {
			totalTime += segment.durationSeconds;

			if (Type.BLOCK == segment.type) {
				npCalc.register(segment.power, segment.durationSeconds);
				
				if (segment.power >= WorkoutHelper.z6) {
					z6Time += segment.durationSeconds;
				} else if (segment.power >= WorkoutHelper.z5) {
					z5Time += segment.durationSeconds;
				} else if (segment.power >= WorkoutHelper.z4) {
					z4Time += segment.durationSeconds;
				} else if (segment.power >= WorkoutHelper.z3) {
					z3Time += segment.durationSeconds;
				} else if (segment.power >= WorkoutHelper.z2) {
					z2Time += segment.durationSeconds;
				} else if (segment.power >= WorkoutHelper.z1) {
					z1Time += segment.durationSeconds;
				}
			} else if (Type.RAMP == segment.type) {
				int span = segment.power2 - segment.power;
				for (int i = 1; i <= segment.durationSeconds; i++) {
					float perc = i / (float) segment.durationSeconds;
					float prog = perc * span;
					int instPower = Math.round(prog + segment.power);
					npCalc.register(instPower);

					if (instPower >= WorkoutHelper.z6) {
						z6Time++;
						segment.addTimeInZone(6, 1);
					} else if (instPower >= WorkoutHelper.z5) {
						z5Time++;
						segment.addTimeInZone(5, 1);
					} else if (instPower >= WorkoutHelper.z4) {
						z4Time++;
						segment.addTimeInZone(4, 1);
					} else if (instPower >= WorkoutHelper.z3) {
						z3Time++;
						segment.addTimeInZone(3, 1);
					} else if (instPower >= WorkoutHelper.z2) {
						z2Time++;
						segment.addTimeInZone(2, 1);
					} else if (instPower >= WorkoutHelper.z1) {
						z1Time++;
						segment.addTimeInZone(1, 1);
					}
				}
				
				Iterator<TimeInZone> tizIter = segment.durationInZones.iterator();
				while(tizIter.hasNext()) {
					TimeInZone tiz = tizIter.next();
					if(tiz.time < 10) {
						//Super-short time-in-zones cause problems when drawing the visualization. Kick them out if below an arbitrarily short timespan.
						tizIter.remove();
					}
				}
			}
		}
		
		int np = npCalc.getNormalizedPower();
		float intensityFactor = np / 100f;
		tss = Math.round((totalTime * np * intensityFactor) / (100 * 3600) * 100);
		System.out.println(" TSS: " + tss);
	}
	
	public String getTssString() {
		if(includesFreeride()) {
			return String.valueOf(tss) + "+";
		} else {
			return String.valueOf(tss);
		}
	}
	
	private boolean includesFreeride() {
		for(WorkoutSegment segment : segments) {
			if(segment.type == Type.FREERIDE) {
				return true;
			}
		}
		
		return false;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();

		str.append("-".repeat(name.length()));
		str.append("\n");
		str.append(name);
		str.append("\n");
		str.append("-".repeat(name.length()));

		str.append("\n");
		str.append("Duration: ");
		str.append(WorkoutHelper.friendlyDuration(totalTime));

		str.append("\n");
		str.append("      Z1: ");
		str.append(WorkoutHelper.friendlyDuration(z1Time));
		str.append("\n");
		str.append("      Z2: ");
		str.append(WorkoutHelper.friendlyDuration(z2Time));
		str.append("\n");
		str.append("      Z3: ");
		str.append(WorkoutHelper.friendlyDuration(z3Time));
		str.append("\n");
		str.append("      Z4: ");
		str.append(WorkoutHelper.friendlyDuration(z4Time));
		str.append("\n");
		str.append("      Z5: ");
		str.append(WorkoutHelper.friendlyDuration(z5Time));
		str.append("\n");
		str.append("      Z6: ");
		str.append(WorkoutHelper.friendlyDuration(z6Time));

		return str.toString();
	}
}
