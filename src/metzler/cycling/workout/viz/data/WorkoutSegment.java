package metzler.cycling.workout.viz.data;

import java.util.ArrayList;
import java.util.List;

public class WorkoutSegment {
	public enum Type {
		RAMP, BLOCK, INTERVALS, FREERIDE;
	}

	//All types
	public Type type = null;
	public int durationSeconds = 0;
	public int power = 0;
	
	//Ramp and intervals only
	public int power2 = 0;

	//Ramp only
	public List<TimeInZone> durationInZones = null;
	public void addTimeInZone(int zone, int durationSeconds) {
		//Ensure that the list is instantiated
		if(durationInZones == null)
			durationInZones = new ArrayList<>();
		
		//Ensure that the current zone is accurate
		if(durationInZones.isEmpty() || durationInZones.get(durationInZones.size() - 1).zone != zone)
			durationInZones.add(new TimeInZone(zone));
		
		//Add time to the zone
		durationInZones.get(durationInZones.size() - 1).time += durationSeconds;
	}
	
	//Intervals only
	public int repeat = 0;
	public int durationOn = 0;
	public int durationOff = 0;
	public List<WorkoutSegment> blockizeTheIntervals() {
		//Blockize the intervals for drawing purposes
		List<WorkoutSegment> blockized = new ArrayList<>();
		
		for(int i = 0; i < repeat; i++) {
			WorkoutSegment onSegment = new WorkoutSegment();
			onSegment.type = Type.BLOCK;
			onSegment.durationSeconds = durationOn;
			onSegment.power = power;
			blockized.add(onSegment);
			
			WorkoutSegment offSegment = new WorkoutSegment();
			offSegment.type = Type.BLOCK;
			offSegment.durationSeconds = durationOff;
			offSegment.power = power2;
			blockized.add(offSegment);
		}
		
		return blockized;
	}
}
