package metzler.cycling.workout.viz.data;

import java.io.FileInputStream;
import java.nio.file.Path;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import metzler.cycling.workout.viz.data.WorkoutSegment.Type;

public class WorkoutReader {
	private static Workout workout = null;

	public static Workout readWorkout(Path workoutFile) {
		try {
			workout = new Workout();

			XMLEventReader reader = XMLInputFactory.newInstance()
					.createXMLEventReader(new FileInputStream(workoutFile.toFile()));
			while (reader.hasNext()) {
				XMLEvent event = reader.nextEvent();
				if (event.isStartElement()) {
					StartElement start = event.asStartElement();
					if ("workout_file".equals(start.getName().getLocalPart())) {
						parseWorkoutFileTag(reader);
					}
				}
			}

			workout.finish();
			return workout;
		} catch (Exception e) {
			e.printStackTrace(System.out);
			return null;
		}
	}

	private static void parseWorkoutFileTag(XMLEventReader reader) throws XMLStreamException {
		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();
			if (event.isStartElement()) {
				StartElement start = event.asStartElement();
				if ("name".equals(start.getName().getLocalPart())) {
					event = reader.nextEvent();
					if (event.isCharacters()) {
						workout.name = event.asCharacters().getData();
					}
				} else if ("description".equals(start.getName().getLocalPart())) {
					event = reader.nextEvent();
					if (event.isCharacters()) {
						workout.description = event.asCharacters().getData().replaceAll("<.*?>|\\n|\\r", " ").replaceAll("\\s+", " ").trim();
					}
				} else if ("workout".equals(start.getName().getLocalPart())) {
					parseWorkoutTag(reader);
				}
			}
		}
	}

	private static void parseWorkoutTag(XMLEventReader reader) throws XMLStreamException {
		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();
			if (event.isStartElement()) {
				StartElement start = event.asStartElement();
				String elementName = start.getName().getLocalPart();
				if ("SteadyState".equalsIgnoreCase(elementName)) {
					WorkoutSegment segment = new WorkoutSegment();
					segment.type = Type.BLOCK;
					segment.durationSeconds = durationFloatToInt(start.getAttributeByName(q("Duration")).getValue());
					segment.power = powerFloatToInt(start.getAttributeByName(q("Power")).getValue());
					workout.segments.add(segment);
				} else if ("Ramp".equalsIgnoreCase(elementName) || "Warmup".equalsIgnoreCase(elementName) || "Cooldown".equalsIgnoreCase(elementName)) {
					WorkoutSegment segment = new WorkoutSegment();
					segment.type = Type.RAMP;
					segment.durationSeconds = durationFloatToInt(start.getAttributeByName(q("Duration")).getValue());
					segment.power = powerFloatToInt(start.getAttributeByName(q("PowerLow")).getValue());
					segment.power2 = powerFloatToInt(start.getAttributeByName(q("PowerHigh")).getValue());
					workout.segments.add(segment);
				} else if ("IntervalsT".equalsIgnoreCase(elementName)) {
					int repeat = Integer.parseInt(start.getAttributeByName(q("Repeat")).getValue());
					int onDurationSeconds = durationFloatToInt(start.getAttributeByName(q("OnDuration")).getValue());
					int offDurationSeconds = durationFloatToInt(start.getAttributeByName(q("OffDuration")).getValue());
					int onPower = powerFloatToInt(start.getAttributeByName(q("OnPower")).getValue());
					int offPower = powerFloatToInt(start.getAttributeByName(q("OffPower")).getValue());
					
					WorkoutSegment segment = new WorkoutSegment();
					segment.type = Type.INTERVALS;
					segment.durationSeconds = (onDurationSeconds + offDurationSeconds) * repeat;
					segment.durationOn = onDurationSeconds;
					segment.durationOff = offDurationSeconds;
					segment.repeat = repeat;
					segment.power = onPower;
					segment.power2 = offPower;
					workout.segments.add(segment);
				} else if ("FreeRide".equalsIgnoreCase(elementName)) {
					WorkoutSegment segment = new WorkoutSegment();
					segment.type = Type.FREERIDE;
					segment.durationSeconds = durationFloatToInt(start.getAttributeByName(q("Duration")).getValue());
					workout.segments.add(segment);
				} else if ("textevent".equalsIgnoreCase(elementName)) {
					//Don't do anything with these
				} else {
					System.out.println("UNKNOWN ELEMENT: " + elementName);
				}
			}
		}
	}

	private static int durationFloatToInt(String raw) {
		//For some reason some workouts have non-integers for duration (ex: 180.00002)
		return Integer.parseInt(raw.replaceFirst("\\..+", ""));
	}
	
	private static int powerFloatToInt(String raw) {
		float flt = Float.parseFloat(raw) * 100;
		return Math.round(flt);
	}

	private static QName q(String localName) {
		return new QName(null, localName, "");
	}
}
