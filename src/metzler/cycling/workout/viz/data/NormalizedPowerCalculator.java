package metzler.cycling.workout.viz.data;

import java.util.ArrayList;
import java.util.List;

public class NormalizedPowerCalculator {
	private final int rollingWindowSize = 30;
	
	private int[] rollBuff = new int[rollingWindowSize];
	private int i = 0;
	private int count = 0;
	private List<Double> rollAvgPowered = new ArrayList<>();
	
	public void register(int power) {
		rollBuff[i] = power;
		i++; count++;
		if(i >= rollingWindowSize) {
			i = 0;
		}
		if(count >= rollingWindowSize) {
			float rollingAvg = 0;
			for(int j = 0; j < rollingWindowSize; j++) {
				rollingAvg += rollBuff[j];
			}
			rollingAvg /= rollingWindowSize;
			rollAvgPowered.add(Double.valueOf(Math.pow(rollingAvg, 4)));
		}
	}
	
	public void register(int power, int numSec) {
		for(int i = 0; i < numSec; i++) {
			register(power);
		}
	}
	
	public int getNormalizedPower() {
		double rollAvgAvg = 0;
		
		for(Double rollAvg : rollAvgPowered) {
			rollAvgAvg += rollAvg.doubleValue();
		}
		rollAvgAvg /= rollAvgPowered.size();
		
		return (int)Math.round(Math.pow(rollAvgAvg, 0.25));
	}
}
